<?php

include_once(dirname(dirname(__FILE__)) . '/ZSAPI.php');

class ZSAPI_Controlpanel extends ZSAPI
{
    public $owner;
    public $customerType = 'commercial';
    public $read_only = true;
    public $template = "zaakstad";
    public $shortname;
    public $allowed_instances = 1;
    public $customer_reference;

    public function __construct($params = array()) {
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                if (!property_exists(__CLASS__, $key)) { continue; }

                $this->$key = $value;
            }
        }
    }

    public function create() {
        $params = $this->_checkFields();


        return $this->dispatch('/api/v1/controlpanel/create', $params);
    }

    public function update($cp_id) {
        $params = $this->_checkFields();

        return $this->dispatch('/api/v1/controlpanel/' . $cp_id . '/update',$params);
    }

    private function _checkFields() {
        if (!$this->owner || !$this->customerType) {
            throw new Exception('Required fields $owner or $customerType are missing');
        }

        $params = array(
            'owner'             => $this->owner,
            'customer_type'     => $this->customerType,
            'allowed_instances' => $this->allowed_instances,
        );

        if ($this->read_only) {
            $params['read_only'] = $this->read_only;
        }

        if ($this->shortname) {
            $params['shortname'] = $this->shortname;
        }

        if ($this->template) {
            $params['template']  = $this->template;
        }

        if ($this->customer_reference) {
            $params['customer_reference'] = $this->customer_reference;
        }

        return $params;
    }

    public function is_host_available($host) {
        $available = $this->dispatch(
            '/api/v1/controlpanel/host_available',
            array('customer_type' => $this->customerType, 'fqdn' => $host)
        );

        if ($available->type == 'freeform' && $available->instance['available']) {
            return true;
        }

        return false;
    }
}

?>
