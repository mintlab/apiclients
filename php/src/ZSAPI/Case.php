<?php

include_once(dirname(dirname(__FILE__)) . '/ZSAPI.php');

class ZSAPI_Case extends ZSAPI
{

    /** @var uuid casetype */
    public $casetype;

    /** @var string source */
    public $source = 'webformulier';

    public $values = array();
    public $requestor = array();

    public function __construct($params = array()) {
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                if (!property_exists(__CLASS__, $key)) { continue; }

                $this->$key = $value;
            }
        }
    }

    public function create() {
        $this->_checkFields();

        $params = array(
            'source'        => $this->source,
            'requestor'     => $this->requestor,
            'values'        => $this->_prepare_values($this->values),
            'casetype_id'   => $this->casetype,
        );

        return $this->dispatch('/api/v1/case/create',$params);
    }

    public function update($case_id) {
        $this->_checkFields();

        $params = array(
            'values'        => $this->_prepare_values($this->values),
        );

        return $this->dispatch('/api/v1/case/' . $case_id . '/update',$params);
    }

    public function transition($case_id, $result_id) {
        $params = array(
            'result' => 'afgehandeld'
        );

        return $this->dispatch('/api/v1/case/' . $case_id . '/transition',$params);   

    }

    private function _prepare_values($values) {
        if (!$values || !is_array($values)) {
            throw new Exception('Invalid values given, make sure you send a hash');
        }

        $rv = array();
        foreach ($values as $key => $value) {
            $rv[$key] = array($value);
        }

        return $rv;

    }

    private function _checkFields() {
        if (!$this->casetype || !$this->source || !$this->requestor) {
            throw new Exception('Required fields casetype, source or requestor are missing');
        }

        if (!$this->requestor['type']) {
            throw new Exception('Required option "type" is missing for requestor');
        }

        if (!count($this->values)) {
            throw new Exception('Every case starts with at least one value');
        }
    }
}

?>
