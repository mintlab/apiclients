<?php

require_once(dirname(__FILE__).  '/../ZSAPI/Controlpanel.php');
require_once(dirname(__FILE__). '/../ZSAPI/Instance.php');
require_once(dirname(__FILE__). '/../ZSAPI/Subject.php');
require_once(dirname(__FILE__) . '/../ZSAPI/Case.php');

global $_zsapi_config;
function _zsapi_get_config() {
    global $_zsapi_config;
    
    if ($_zsapi_config) { return $_zsapi_config; }

    $ini_file       = dirname(__FILE__) . '/../../../../zsapi_config.ini';
    $_zsapi_config  = parse_ini_file($ini_file, true);

    if (!$_zsapi_config) { die('Cannot find file: ' . $ini_file); }

    return $_zsapi_config;
}

function zsapi_post_params($input) {
    $zsapi_config       = _zsapi_get_config();

    $authcredentials    = array(
        'apiHostname' => $zsapi_config['credentials']['api_url'],
        'apiUsername' => $zsapi_config['credentials']['api_username'],
        'apiPassword' => $zsapi_config['credentials']['api_password'],
        'apiIdentifier' => $zsapi_config['credentials']['api_identifier'],
    );

    $authcredentials_cp    = $authcredentials;
    $authcredentials_cp['apiIdentifier'] = $zsapi_config['credentials']['api_identifier_controlpanel'];

    $bytes = openssl_random_pseudo_bytes(4);
    $password = bin2hex($bytes);

    ### { Create subject
    $s      = new ZSAPI_Subject     ($authcredentials, array( 'subject' => $input['subject']));
    $cp     = new ZSAPI_Controlpanel($authcredentials_cp);

    $s->subject_params['authentication'] = array(
        'password' => $password,
        'username' => $input['subject']['coc_number']
    );

    $s->subject_params['subject']['address_residence']['zipcode'] = strtoupper(
        $s->subject_params['subject']['address_residence']['zipcode']
    );


    $s->subject_type    = 'company';
    if (!$s->is_subject_available()) {
        error_log('Host already exists' . var_export($s->log(), 1));
        return;
    }

    if (!$cp->is_host_available($input['cp']['fqdn'])) {
        error_log('Host already exists' . var_export($cp->log(), 1));
        return;
    }

    $response = null;
    try {
        $response = $s->create();
    } catch (Exception $e) {}

    if (!$response || $response->is_error) {
        error_log('Failed to create subject, stop posting: ' . $e . var_export($s->log(), 1));
        return;
        ### Stop
    }

    ### } END Create subject

    ### { Create controlpanel
    $cp->owner = $response->reference;

    ### Define a short_name
    $cp->shortname = substr(strtolower(preg_replace("/[^a-zA-Z0-9]+/","", $input['subject']['company'])),0,8);

    $response = null;
    try {
        $response = $cp->create();
    } catch (Exception $e) {
        error_log('Failed to create controlpanel, stop posting: ' . $e . var_export($cp->log(), 1));
        return;
    }

    if (!$response || $response->is_error) {
        $cp->log();
        return;
        ### Stop
    }

    $controlpanel_reference = $response->reference;

    ### } END Create controlpanel

    ### { Create instance
    $i = new ZSAPI_Instance(array_merge($authcredentials_cp, array('controlpanel' => $response->reference, 'label' => 'Mintlab proeftuin', 'fqdn' => $input['cp']['fqdn'])));

    $i->password = $password;

    $response = null;
    try {
        $response = $i->create();
    } catch (Exception $e) {}

    if (!$response || $response->is_error) {
        error_log('Failed to create instance, stop posting: ' . $e . var_export($i->log(), 1));
        return;
        ### Stop
    }

    ### } END Create instance
    $casetype_uuid = $zsapi_config['general']['casetype_id'];

    $case_values = $input['case'];
    $case_values['contactpersoon']      = $input['case']['voornaam'] . ' ' . $input['case']['achternaam'];
    $case_values['bedrijfsnaam']        = $input['subject']['company'];

    ### { Create case
    $c = new ZSAPI_Case(
        array_merge(
            $authcredentials,
            array(
                'values'        => $case_values,
                'source'        => $zsapi_config['general']['contactchannel'],
                'casetype'      => $casetype_uuid,
                'requestor'     => array(
                    'type'  => 'company',
                    'id'    => array(
                        'kvk_number'    => $input['subject']['coc_number'],
                        'branch_number' => $input['subject']['coc_location_number'],
                    )
                )
            )
        )
    );


    $response = null;
    try {
        $response = $c->create();
    } catch (Exception $e) {
        error_log('Failed to create case, stop posting: ' . $e . var_export($c->log(), 1));
    }

    if (!$response || $response->is_error) {
        error_log('Failed to create case, stop posting: ' . $e . var_export($c->log(), 1));
        return false;
        ### Stop
    }

    $c->values['proeftuin_locatie']   = 'https://' . $input['cp']['fqdn'] . '.zaaksysteem.net/';
    $c->values['proeftuin_loginnaam'] = 'beheerder';
    $c->values['proeftuin_aangemaakt'] = 'Ja';
    $c->values['proeftuin_wachtwoord'] = $password;

    ### Update case
    try {
        $response = $c->update($response->reference);
    } catch (Exception $e) {
        error_log('Failed to update case, stop posting: ' . $e . var_export($c->log(), 1));
        return false;
    }

    ### Update controlpanel
    $cp->customer_reference = 'case-' . $response->reference;
    try {
        $response = $cp->update($controlpanel_reference);
    } catch (Exception $e) {
        error_log('Failed to update case, stop posting: ' . $e . var_export($c->log(), 1));
        return false;
    }

    ### } END Create case
}

?>
