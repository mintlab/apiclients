<?php

class ZSAPITest extends PHPUnit_Framework_TestCase
{

    public function testVersion()
    {
        $a = new ZSAPI();

        $this->assertNotEmpty($a->getVersion(), 'Got a version for JAPI');
    }

    public function testDispatch()
    {
        $a = new ZSAPI();
        $a->apiHostname = "https://mintlab.zaaksysteem.nl/";

        $a->dispatch('/api/v1/case', array("test" => "blaat"));

        // var_dump($a->log());


        // $this->assertNotEmpty($a->dispatch(array("test" => "blaat")), 'Got a curl instance');
    }
}
