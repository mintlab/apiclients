<?php

include_once(dirname(dirname(__FILE__)).  '/src/ZSAPI/Controlpanel.php');
include_once(dirname(dirname(__FILE__)). '/src/ZSAPI/Instance.php');
include_once(dirname(dirname(__FILE__)). '/src/ZSAPI/Subject.php');
include_once(dirname(dirname(__FILE__)). '/src/ZSAPI/Case.php');

class ZSAPITest extends PHPUnit_Framework_TestCase
{
    public function testWordpress() {

        ### UNCOMMENT return line to make this work
        // return false;

        $input = array(
            'case'  => array(
                'voornaam'                      => 'Michiel',
                'achternaam'                    => 'Ootjers',
                'contactpersoon_email'          => 'michiel@ootjers.nl',
                'contactpersoon_telefoon'       => '0634567890',
            ),
            'subject' => array(
                'coc_location_number'           => '123456789013',
                'coc_number'                    => '37103701',
                'company'                       => 'Mintlab B.V.',
                'address_residence'             => array(
                    'street'                => 'Donker Curtiusstraat',
                    'street_number'         => '7',
                    'street_number_letter'  => 'a',
                    'street_number_suffix'  => 'unit 521',
                    'zipcode'               => '1051JL',
                    'city'                  => 'Amsterdam',
                    'country'               => array(
                        'dutch_code'    => '6030',
                    ),
                ),
            ),
            'cp'      => array(
                'fqdn'  => 'test18'
            )
        );

        zsapi_post_params($input);
    }

}

require_once(dirname(dirname(__FILE__)) . '/src/wordpress/functions.zsapi.php');

    
